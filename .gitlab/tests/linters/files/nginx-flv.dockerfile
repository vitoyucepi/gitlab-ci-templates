FROM nginx:1.24.0-alpine as builder
COPY builder/rootfs /
RUN builder.sh prepare
COPY --chown=abuild:abuild builder/abuild /home/abuild
USER abuild
RUN builder.sh build

FROM nginx:1.24.0-alpine

ENV TZ="UTC"

ENV NGINX_VERSION=1.21.0

COPY --from=builder /home/abuild/.abuild/*.rsa.pub /etc/apk/keys/
COPY --from=builder /home/abuild/packages/abuild/ /packages/

RUN set -eux; \
      echo '/packages' >> /etc/apk/repositories; \
      apk add --no-cache \
        nginx-mod-http-flv="${NGINX_VERSION}-r99" \
      ; \
      rm -rf /packages; \
      find / -type d -name __pycache__ -prune -exec rm -rf {} \;; \
      rm -rf /root/.cache;
